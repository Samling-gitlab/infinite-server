<?php
/**
 * @File:   index.php
 * @Author: Haozhe Xie
 * @Date:   2024-02-01 18:08:07
 * @Last Modified by: Haozhe Xie
 * @Last Modified at: 2024-02-14 09:15:03
 * @Email:  root@haozhexie.com
 */

error_reporting(0);
require_once('helpers.php');

header('Access-Control-Allow-Origin: *');

$request_uri   = $_SERVER["REQUEST_URI"] ?? "";
$request_url   = parse_url($request_uri);
$request_paths = explode("/", $request_url["path"]);
$request_path  = "/" . end($request_paths);

$config = json_decode(@file_get_contents(dirname(__FILE__) . "/../configs/web-server.json"), true);
// Simple Router
switch ($request_path) {
    case "/":
        $html = @file_get_contents(dirname(__FILE__) . "/../default.html");
        foreach ($config as $k => $v) {
            if (is_array($v)) {
                continue;
            }
            if (is_bool($v)) {
                $v = $v ? "true" : "false";
            }
            $html = str_replace(sprintf("%%%s%%", $k), $v, $html);
        }
        echo $html;
        break;
    case "/servers":
        echo json_encode(get_server_info($config));
        break;
    case "/status":
        if ($config["sse"]) sse_loop($config);
        else echo json_encode(get_server_status($config));
        break;
    case "/push":
        $name = $_POST["name"];
        $token = $_POST["token"];
        $output_file_path = sprintf(dirname(__FILE__) . "/../%s/%s.json", $config["status-dir"], $token);

        if (!array_key_exists($name, $config["servers"]) ||
            $config["servers"][$name]["url"] != $token) {
            http_response_code(403);
            break;
        }
        if (array_key_exists("time", $_POST)) {
            dump_server_status($output_file_path, $config, $_POST);
        } else {
            dump_server_info($output_file_path, $config, $_POST);
        }
        break;
    default:
        http_response_code(404);
        break;
}
