<?php
/**
 * @File:   helpers.php
 * @Author: Haozhe Xie
 * @Date:   2024-02-01 18:18:36
 * @Last Modified by: Haozhe Xie
 * @Last Modified at: 2024-02-11 23:18:58
 * @Email:  root@haozhexie.com
 */

function get_server_info($cfg) {
    $info = array();
    foreach ($cfg["servers"] as $name => $values) {
        $server = json_decode(
            @file_get_contents(get_server_url($cfg, $values["url"], "s")),
            true
        );
        if ($server) {
            foreach ($server as $k => $v) {
                if (in_array($k, $cfg["keys"])) {
                    $info[$name][$k] = $v;
                }
            }
            foreach ($values as $k => $v) {
                if ($k == "url") {
                    continue;
                }
                $info[$name][$k] = $v;
            }
        }
    }
    return $info;
}

function get_server_status($cfg) {
    $status = array();
    foreach ($cfg["servers"] as $name => $values) {
        $statusp[$name] = array();
        $_status = json_decode(
            @file_get_contents(get_server_url($cfg, $values["url"], "r")),
            true
        );
        foreach ($_status as $k => $v) {
            if (in_array($k, $cfg["keys"])) {
                $status[$name][$k] = $v;
            }
        }
    }
    return $status;
}

function get_server_url($cfg, $url, $type) {
    if (str_starts_with($url, "http")) {
        $url = sprintf("%s?k=%s", $url, $type);
    } else {
        $url = sprintf(dirname(__FILE__) . "/../%s/%s.%s.json", $cfg["status-dir"], $url, $type);
    }
    return $url;
}

function sse_loop($cfg) {
    header("Cache-Control: no-store");
    header("Content-Type: text/event-stream");
    header('X-Accel-Buffering: no');
    
    // The event loop
    while (true) {
        echo 'data: ' . json_encode(get_server_status($cfg)) . "\n\n";
        ob_end_flush();
        flush(); 
        // Break the loop if the client aborted the connection (closed the page)
        if (connection_aborted()) {
            break;
        }
        sleep($cfg["interval"]);
    }
}

function dump_server_info($output_fname, $cfg, $info) {
    foreach ($info as $k => $v) {
        if (!in_array($k, $cfg["keys"])) {
            unset($info[$k]);
        }
    }
    file_put_contents(
        str_replace(".json", ".s.json", $output_fname),
        json_encode($info)
    );
}

function dump_server_status($output_fname, $cfg, $status) {
    foreach ($status as $k => $v) {
        if (!in_array($k, $cfg["keys"])) {
            unset($status[$k]);
        }
    }
    file_put_contents(
        str_replace(".json", ".r.json", $output_fname),
        json_encode($status)
    );
}
