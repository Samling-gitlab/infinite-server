# Infinite Server

## Introduction

**Infinite Server** is an open-source, super lightweight, and user-friendly server monitoring and operations tool.

Compared to existing server monitoring tools like [Nezha](https://nezha.wiki/) and [MyNodeQuery](https://hub.docker.com/r/jaydenlee2019/mynodequery), Infinite Server stands out with the following highlights.

- No root privileges required.
- Super lightweight: no database or daemon process needed.
- Easy to use, easy to go: Delivered with simple scripts, no need for a complex installer or Docker.

![Preview](https://i.imgur.com/MpvPg89.png)

## Prerequisites

Make sure that you have the web server (Apache or Nginx) and PHP installed.
If not, please refer to the [tutorial](https://www.linode.com/docs/guides/install-php-8-for-apache-and-nginx-on-ubuntu/).

## Installation

Assume that the HTML root directory is `/var/www/html`.

### Agent

There are two **alternative** methods to provide server information: `Push` and `Pull`.

#### Pull

**For all agents using the pull method, the PHP script providing server information should be accessible to the public on the Internet.**

```bash
cd /var/www/html
wget https://gitlab.com/hzxie/infinite-server/-/raw/master/api/status.php
```

The script should be accessiable via `http://YOUR_SERVER_IP/status.php`.

#### Push

To enable server status pushing, the `configs/push-server.json` file needs to be adjusted to correspond with the entries in `configs/web-server.json`.

```json
{
    "name": "Awesome Server Name",
    "token": "RAMDOM-TOKEN-zvjr37m",
    "url": "http://localhost/push"
}
```

The following command should be executed on the server:

```bash
# Server Information (OS, CPU, etc.)
php api/status.php s configs/push-server.json
# Server Status (RAM, Disk, Network, etc.)
while true
do
    php api/status.php r configs/push-server.json
    sleep 15
done
```

To automate the execution of `status.php`, you can set up a daemon configuration at `/lib/systemd/system/srv-status.service` to update the server information automatically every 15 seconds.

```ini
[Unit]
Description=Server Status Daemon
After=network.target nss-lookup.target

[Service]
Type=simple
User=root
NoNewPrivileges=true
ExecStart=/path/to/api/status.php r
Restart=always
RestartSec=15s

[Install]
WantedBy=multi-user.target
```

Note that `/path/to/api/status.php` need to be updated to the location of `status.php` on your server.

The following commands make the execution of `status.php` automatically.

```bash
sudo systemctl enable srv-status
sudo systemctl start srv-status
```

### Dashboard

To set up the dashboard that summarizes all server information, please follow these steps.

#### Clone the Git Repo

```bash
cd /var/www/html
git clone https://gitlab.com/hzxie/infinite-server
```

#### Edit the Servers Node Config

Edit the server node config file `configs/web-server.json`.

```json
{
    "Awesome Server Name": {
        "region": "HK",
        "url": "http://localhost/status.php"
    }
}
```

If the server doesn't have a public IPv4/v6 address, the server's `URL` can be modified to:

```
"url": "RAMDOM-TOKEN-zvjr37m"
```

This adjustment signifies that the server's status will be stored in files prefixed with this prefix by pushing to the web server.

#### Web Server Configuration

If you are using **Apache** web server, add the following content to `.htaccess`.

```apache
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [L]
```

If you are using **Nginx** web server, add the following content to your Nginx config.

```nginx
location / {
    proxy_http_version          1.1;
    proxy_buffering             off;
    try_files                   $uri /index.php$is_args$args;
}
```

The web page should be accessiable via `http://YOUR_SERVER_IP/infinite-server`.

## License

The project is open sourced under the MIT license.
