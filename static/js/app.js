/*
 * @File:   app.js
 * @Author: Haozhe Xie
 * @Date:   2024-02-03 11:25:18
 * @Last Modified by: Haozhe Xie
 * @Last Modified at: 2024-02-14 09:17:21
 * @Email:  root@haozhexie.com
 */

function getHumanizeFileSize(size) {
	size = parseInt(size)
	if (size > 1073741824) {
		return (size / 1073741824).toFixed(1) + "G"
	} else if (size > 1048576) {
		return (size / 1048576).toFixed(1) + "M"
	} else {
		return (size / 1024).toFixed(1) + "K"
	}
}

function getTimeElapsedSince(timestamp) {
	var seconds = Math.floor(new Date().getTime() / 1000 - new Date(timestamp)),
		interval = Math.floor(seconds / 31536000);
	
	if (interval > 1) return interval + "y";

	interval = Math.floor(seconds / 2592000);
	if (interval > 1) return interval + "m";

	interval = Math.floor(seconds / 86400);
	if (interval >= 1) return interval + "d";

	interval = Math.floor(seconds / 3600);
	if (interval >= 1) return interval + "h";

	interval = Math.floor(seconds / 60);
	if (interval > 1) return interval + "m ";
	return Math.floor(Math.max(0, seconds)) + "s";
}

function getNetworkSpeed(prev_values, curr_values, tsmt_key, ts_key) {
	return getHumanizeFileSize(
		(curr_values[tsmt_key] - prev_values[tsmt_key]) /
		(curr_values[ts_key] - prev_values[ts_key] + 0.01) *
		1000) + "/s"
}

function getServerStatus(status, servers) {
	Object.entries(status).forEach(entry => {
		const [name, _status] = entry
		if (name in servers) {
			let _server = toRaw(servers[name])
			Object.entries(_status).forEach(entry => {
				let [k, v] = entry
				// Estimate the network speed
				if (k == "netdev" && "ts" in _server["netdev"]) {
					v["txs"] = getNetworkSpeed(v, _server[k], "rx", "ts")
					v["rxs"] = getNetworkSpeed(v, _server[k], "tx", "ts")
				} else if (k == "loadavg") {
					for (let i = 0; i < v.length; ++ i) {
					v[i] *= 100 / _server["cpuinfo"]["num"]
					}
				}
				servers[name][k] = v
			})
			_server["bgclass"] = getProgressBarBgClasses(_server)
		}
	})
	return servers;
}

function getProgressBarBgClasses(status) {
	let bgClasses = {}
	Object.entries(status).forEach(entry => {
		let [k, v] = entry
		if (k == "loadavg") {
			bgClasses["load"] = getProgressBarBgClass(v[0])
		} else if (k == "meminfo") {
			bgClasses["memUsed"] = getProgressBarBgClass(v["memUsedPercent"])
			bgClasses["swapUsed"] = getProgressBarBgClass(v["swapPercent"])
		} else if (k == "diskinfo") {
			bgClasses["diskUsed"] = getProgressBarBgClass(v["diskPercent"])
		}
	})
	return bgClasses
}

function getProgressBarBgClass(value) {
	if (value >= 90) {
		return "bg-danger"
	} else if (value >= 75) {
		return "bg-warning"
	} else {
		return "progress-bar"
	}
}

function getCurrentTheme() {
	let isSystemDark = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches,
		currenetTheme = localStorage.getItem("theme")

	return currenetTheme ? currenetTheme : (isSystemDark ? "dark" : "light")
}

function setDarkMode(theme) {
	let icon = document.getElementById("dark-mode-switch").querySelector("i")

	icon.classList = "fa"
	document.querySelector("html").setAttribute("data-theme", theme)
	localStorage.setItem("theme", theme)
	if (theme == "dark") {
		icon.classList.add("fa-sun")
	} else {
		icon.classList.add("fa-moon")
	}
}
